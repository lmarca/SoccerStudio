//
//  ShowAlertController.swift
//  Culture
//
//  Created by J on 3/19/16.
//  Copyright © 2016 LIMAAPP E.I.R.L. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showError(_ error:NSError) {
        
        showToast(error.localizedDescription)
    }
    
    func showToast(_ msg:String) {
        
        let alertController = UIAlertController(title: msg, message: nil, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
        
    }
}
