//
//  myActivity.swift
//  Pulso
//
//  Created by Procesos on 16/11/16.
//  Copyright © 2016 Luis Marca Zapata. All rights reserved.
//

import UIKit

class myActivity: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func navigationItemTitle(title : String) {
        
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 15)!, NSForegroundColorAttributeName: UIColor.white]
        self.navigationItem.title = title
    }
    
    func toast(_ msg : String) {
        let alertController = UIAlertController(title: msg, message: nil, preferredStyle: .alert)
        self.present(alertController, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            // your code here
            alertController.dismiss(animated: true, completion: nil)
        }
    }
    
    func formatButton(_ button: UIButton) {
        
        button.layer.cornerRadius = 25.0
        button.layer.masksToBounds = true
        button.layer.borderWidth = 1.0
        button.layer.borderColor = UIColor.white.cgColor
    }
    
    func borderUITextField(_ textField: UITextField, _ color: UIColor) {
        
        textField.layer.masksToBounds = true
        textField.layer.borderColor = color.cgColor
        textField.layer.borderWidth = 1.0
        textField.layer.cornerRadius = 4.0
    }
    
    func getLabelHeight(label: UILabel, text: String) -> CGFloat {
        
        let nsText = text as NSString
        
        let constraint = CGSize.init(width: label.frame.size.width, height: CGFloat.greatestFiniteMagnitude)
        let context = NSStringDrawingContext()
        let boundingBox = nsText.boundingRect(with: constraint, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: label.font], context: context).size
        
        return CGSize.init(width: ceil(boundingBox.width), height: ceil(boundingBox.height)).height
    }
    
    func atributeTextoHightSpacing(nsText: String) -> NSMutableAttributedString{
        
        let attrString = NSMutableAttributedString()
        attrString.append(stringFromHtml(string: nsText)!)
        
        let textRange = NSMakeRange(0, attrString.length)
        attrString.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: textRange)
        attrString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 13), range: textRange)
        
        return attrString
    }
    
    public func stringFromHtml(string: String) -> NSAttributedString? {
        do {
            let data = string.data(using: String.Encoding.unicode, allowLossyConversion: true)
            if let d = data {
                
                let str = try NSAttributedString(data: d,
                                                 options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                 documentAttributes: nil)
                
                return str
            }
        } catch {
        }
        return nil
    }
    
    func pop() {
        self.navigationController?.pop(animated: true)
    }
    
    func popToRoot() {
        self.navigationController?.popToRoot(animated: true)
    }

}
/*
extension myActivity : UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return ViewPresentAnimationController()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return ViewDismissAnimationController()
    }
}
*/
extension NotificationCenter {
    func setObserver(_ observer: AnyObject, selector: Selector, name: NSNotification.Name, object: AnyObject?) {
        NotificationCenter.default.removeObserver(observer, name: name, object: object)
        NotificationCenter.default.addObserver(observer, selector: selector, name: name, object: object)
    }
}

extension myActivity {

    func setTabBarVisible(isVisible: Bool) {
        
        self.tabBarController?.tabBar.isHidden = isVisible
        self.tabBarController?.tabBar.isTranslucent = isVisible
    }
    
    func show (_ vc: String) {
    
        DispatchQueue.main.async {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: vc)
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    
//    func showPopup (_ vc: UIViewController) {
//
//        DispatchQueue.main.async(execute: {
//
//            vc.modalPresentationStyle = .custom
//            vc.transitioningDelegate = self
//            self .present(vc, animated: true, completion: nil)
//        })
//    }
    
    func verifyUrl (urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    func setAttributedString(text: String, filter: String) -> NSMutableAttributedString {
        
        let nsText = text as NSString
        let textRange = NSMakeRange(0, nsText.length)
        let attributedString = NSMutableAttributedString(string: text)
        
        nsText.enumerateSubstrings(in: textRange, options: .byWords, using: {
            (substring, substringRange, _, _) in
            
            if (substring == filter) {
                attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: substringRange)
                attributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 13), range: substringRange)
            }
        })
        
        return attributedString
    }
    
}

//extension myActivity : UIViewControllerTransitioningDelegate {
//
//    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//
//        return ViewPresentAnimationController()
//    }
//
//    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//
//        return ViewDismissAnimationController()
//    }
//}


extension UINavigationController {
    
    func pop(animated: Bool) {
        _ = self.popViewController(animated: animated)
    }
    
    func popToRoot(animated: Bool) {
        _ = self.popToRootViewController(animated: animated)
    }
    
    func popImagenHidden(animated: Bool) {
        
        if !animated
        {
            self.navigationBar.setBackgroundImage(UIImage(named:"navBar")?.stretchableImage(withLeftCapWidth: 0, topCapHeight: 0), for: .default)
        }
        else
        {
            self.navigationBar.setBackgroundImage(UIImage.fromColor(color: UIColor.backgroundMenu()), for: .default)
        }
        
        self.navigationBar.isTranslucent = true
    }
    
    func retunToSpecificViewController() {
        
    }
    
    func backToViewController(viewController: Swift.AnyClass) {
        
        for element in viewControllers as Array {
            if element.isKind(of: viewController) {
                self.popToViewController(element, animated: true)
                break
            }
        }
    }
}

extension CGRect {
    init(_ x:CGFloat, _ y:CGFloat, _ w:CGFloat, _ h:CGFloat) {
        self.init(x:x, y:y, width:w, height:h)
    }
}

private var __maxLengths = [UITextField: Int]()
extension UITextField {
    
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
                return 150 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    func fix(textField: UITextField) {
        let t = textField.text
        textField.text = t?.safelyLimitedTo(length: maxLength)
    }
}

extension UITextField {
    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSForegroundColorAttributeName: newValue!])
        }
    }
    
    var count:Int {
        get{
            return self.text?.characters.count ?? 0
        }
    }
    
}

extension UILabel {
    
    var count:Int {
        get{
            return self.text?.characters.count ?? 0
        }
    }
}


extension String {
    
    var length: Int { return characters.count    }  // Swift 2.0
    
    func safelyLimitedTo(length n: Int)->String {
        let c = self.characters
        if (c.count <= n) { return self }
        return String( Array(c).prefix(upTo: n) )
    }
    
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return substring(from: fromIndex)
    }
    
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return substring(to: toIndex)
    }
    
    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return substring(with: startIndex..<endIndex)
    }
    
    var canOpenURL : Bool {
        
        guard let url = NSURL(string: self) else {return false}
        if !UIApplication.shared.canOpenURL(url as URL) {return false}
        let regEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[regEx])
        return predicate.evaluate(with: self)
    }
    
    func replace(target: String, withString: String) -> String
    {
        return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
    }
    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    func removingWhiteLastspaces() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
    //trimmingCharacters(in: .whitespacesAndNewlines)
    
    
//    let str = "Hello, playground"
//    print(str.substring(from: 7))         // playground
//    print(str.substring(to: 5))           // Hello
//    print(str.substring(with: 7..<11))    // play
}


@IBDesignable
class DesignableUITextField: UITextField {
    
    // Provides left padding for images
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += leftPadding
        return textRect
    }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0
    
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    
    /// the corner radius value to have a button with rounded corners.
    @IBInspectable open var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    func updateView() {
        if let image = leftImage {
            leftViewMode = UITextFieldViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.image = image
            imageView.contentMode = .scaleAspectFit
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = color
            leftView = imageView
        } else {
            leftViewMode = UITextFieldViewMode.never
            leftView = nil
        }
        
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSForegroundColorAttributeName: color])
    }
}

extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        //formatter.groupingSeparator = " "
        formatter.locale = Locale(identifier: "en_US")
        formatter.numberStyle = .decimal
        return formatter
    }()
}

extension Integer {
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? ""
    }
}

extension Sequence where Iterator.Element == [String: Any] {
    
    func values(of key: String) -> [Any]? {
        let result: [Any] = reduce([]) { (result, dict) -> [Any] in
            var result = result
            result.append(dict.filter{ $0.key == key }.map{ $0.value })
            return result
        }
        return result.isEmpty ? nil : result
    }
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.height
    }
}

extension UIImage {
    static func fromColor(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}

