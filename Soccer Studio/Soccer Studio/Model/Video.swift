//
//  Video.swift
//  Soccer Studio
//
//  Created by APPLE on 6/06/18.
//  Copyright © 2018 Oxicode. All rights reserved.
//

import UIKit

class Video: NSObject {

    var status: String = ""
    var label: String = ""
    var video: URL!
    
    override init() {
        
    }
    
    init(_ status: String, _ label: String, _ url:String) {
        
        self.status = status
        self.label = label
        self.video = URL.init(string: url)
    }
    
}
