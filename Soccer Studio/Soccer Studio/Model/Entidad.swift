//
//  Entidad.swift
//  Soccer Studio
//
//  Created by APPLE on 22/05/18.
//  Copyright © 2018 Oxicode. All rights reserved.
//

import UIKit

class Entidad: NSObject {

    var icon: String = ""
    var nombre: String = ""
    var url: URL!
    
    override init() {
        
    }
    
    init(_ icon: String, _ nombre: String, _ url:String) {
        
        self.icon = icon
        self.nombre = nombre
        self.url = URL.init(string: url)
    }
}
