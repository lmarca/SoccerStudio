//
//  SuscribeVC.swift
//  Soccer Studio
//
//  Created by APPLE on 15/05/18.
//  Copyright © 2018 Oxicode. All rights reserved.
//

import UIKit

class SuscribeVC: myActivity {
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    internal var isModal: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Premium account"
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_flecha_atras"), style: .plain, target: self, action: #selector(SuscribeVC.popToViewController))
        
        // Do any additional setup after loading the view.
    }
    
    func popToViewController() {
        if isModal {
            self.dismiss(animated: true, completion: nil)
        }
        else {
            pop()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
