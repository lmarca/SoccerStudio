//
//  LoginVC.swift
//  Soccer Studio
//
//  Created by APPLE on 15/05/18.
//  Copyright © 2018 Oxicode. All rights reserved.
//

import UIKit
import TransitionButton

class LoginVC: myActivity {

    @IBOutlet weak var imageView: UIImageView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageView.CCAnimationJump(intensity: 100)
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func onClick(_ sender: UIButton) {
        
        switch sender.tag {
        case 2:
            show("RegisterVC")
        case 4:
            show("SuscribeVC")
        default:
            break
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LoginVC : UITextFieldDelegate {
    
    // Hide the keyboard when the return key pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
