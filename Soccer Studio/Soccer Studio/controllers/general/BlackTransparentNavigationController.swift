//
//  BlackTransparentNavigationController.swift
//  Culture
//
//  Created by J on 4/6/16.
//  Copyright © 2016 LIMAAPP E.I.R.L. All rights reserved.
//

import UIKit

class BlackTransparentNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UINavigationBar.appearance().barTintColor = UIColor.backgroundMenu()
        
        navigationBar.tintColor = UIColor.white
        navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont(name: "Avenir-Light", size: 19.0)!]
        
        self.navigationBar.shadowImage = UIImage()
        self.interactivePopGestureRecognizer!.isEnabled = false
        
    }
}
