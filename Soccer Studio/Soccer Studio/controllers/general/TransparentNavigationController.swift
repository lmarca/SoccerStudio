

import UIKit

class TransparentNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont(name: "Avenir-Light", size: 15.0)!]

        navigationBar.setBackgroundImage(UIImage.fromColor(color: .clear), for: .default)
        navigationBar.isTranslucent = true
        navigationBar.tintColor = UIColor.white

        self.navigationBar.shadowImage = UIImage()
        self.interactivePopGestureRecognizer!.isEnabled = false
        
    }
}
