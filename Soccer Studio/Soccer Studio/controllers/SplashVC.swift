//
//  SplashVC.swift
//  Soccer Studio
//
//  Created by APPLE on 15/05/18.
//  Copyright © 2018 Oxicode. All rights reserved.
//

import UIKit
import SKSplashView


class SplashVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let splashIcon = SKSplashIcon(image: UIImage.init(named: "logo-app"), animationType: .blink)
        
        let splashView = SKSplashView(splashIcon: splashIcon!, backgroundColor: .white, animationType: .shrink)
        
        self.view.addSubview(splashView!)
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
