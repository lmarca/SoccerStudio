//
//  DetailHomeVC.swift
//  Soccer Studio
//
//  Created by APPLE on 6/06/18.
//  Copyright © 2018 Oxicode. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class DetailHomeVC: myActivity {
    
    @IBOutlet weak var gridCollectionView: UICollectionView!
    @IBOutlet weak var gridCollectionView1: UICollectionView!
    
    internal var isTopic: Bool = false
    
    var video: Video!
    var hashVideos: [Video] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            gridCollectionView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
        //gridCollectionView.contentInsetAdjustmentBehavior = .automatic
        
        hashVideos.append(Video("1", "Goals", "http://ipfcom.org/soccerstudio/uploads/messi-assists.mp4"))
        hashVideos.append(Video("1", "Assists", "http://ipfcom.org/soccerstudio/uploads/messi_dribbling.mp4"))
        hashVideos.append(Video("1", "Dribbling", "http://ipfcom.org/soccerstudio/uploads/messi_headers.mp4"))
        hashVideos.append(Video("1", "Key passes", "http://ipfcom.org/soccerstudio/uploads/messi_goals.mp4"))
        hashVideos.append(Video("1", "Long shots", "http://ipfcom.org/soccerstudio/uploads/messi_key-passes.mp4"))
        hashVideos.append(Video("1", "Headers", "http://ipfcom.org/soccerstudio/uploads/messi_key-passes.mp4"))
        
        
        if isTopic {
            hashVideos.append(Video("0", "Finishing", "http://ipfcom.org/soccerstudio/uploads/messi_long-shots.mp4"))
            hashVideos.append(Video("0", "Penalties", "http://ipfcom.org/soccerstudio/uploads/messi_long-shots.mp4"))
            hashVideos.append(Video("0", "Free-kick shots", "http://ipfcom.org/soccerstudio/uploads/messi-assists.mp4"))
            hashVideos.append(Video("0", "Crosses", "http://ipfcom.org/soccerstudio/uploads/messi_dribbling.mp4"))
            hashVideos.append(Video("0", "Counter-attacks", "http://ipfcom.org/soccerstudio/uploads/messi_headers.mp4"))
            hashVideos.append(Video("0", "Playmaking", "http://ipfcom.org/soccerstudio/uploads/messi_goals.mp4"))
            hashVideos.append(Video("0", "Throw-ins", "http://ipfcom.org/soccerstudio/uploads/messi_key-passes.mp4"))
            hashVideos.append(Video("0", "Long Pass", "http://ipfcom.org/soccerstudio/uploads/messi_key-passes.mp4"))
            hashVideos.append(Video("0", "Set pieces crosses", "http://ipfcom.org/soccerstudio/uploads/messi_long-shots.mp4"))
            hashVideos.append(Video("0", "Key defendings", "http://ipfcom.org/soccerstudio/uploads/messi_long-shots.mp4"))
            hashVideos.append(Video("0", "Tackling", "http://ipfcom.org/soccerstudio/uploads/messi-assists.mp4"))
            hashVideos.append(Video("0", "Set pieces in defense", "http://ipfcom.org/soccerstudio/uploads/messi_dribbling.mp4"))
            hashVideos.append(Video("0", "Pressing", "http://ipfcom.org/soccerstudio/uploads/messi_headers.mp4"))
            hashVideos.append(Video("0", "Air challenges", "http://ipfcom.org/soccerstudio/uploads/messi_goals.mp4"))
        }
        
        
        setup()
        

        // Do any additional setup after loading the view.
    }
    
    fileprivate func setup() {
        
        gridCollectionView1.register(UINib(nibName: "CollectionViewCellVideo", bundle: nil), forCellWithReuseIdentifier: "Cell")
        gridCollectionView1.dataSource = self
        gridCollectionView1.delegate = self
        
        layoutCells()
    }
    
    func layoutCells() {
        let layout = UICollectionViewFlowLayout()
        //layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 10)
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 1.0
        layout.minimumLineSpacing = 1.0
        layout.itemSize = CGSize(width: 150, height: 150)
        
        gridCollectionView1!.collectionViewLayout = layout
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension DetailHomeVC: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hashVideos.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCellVideo
        
        video = hashVideos[indexPath.row]
        
        //cell.imageView.image = UIImage.init(named: video.video)
        
        if video.status == "1" {
            cell.info.isHidden = true
            cell.icon.image = UIImage.init(named: "play")
        }else{
            cell.info.isHidden = false
            cell.icon.image = UIImage.init(named: "locked")
        }
        
        cell.titulo.text = video.label
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        video = hashVideos[indexPath.row]
        
        if video.status == "1" {
            
            let videoURL = hashVideos[indexPath.row].video
            let player = AVPlayer(url: videoURL!)
            let playerViewController = AVPlayerViewController()
            
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
        else {
            toast("Video no disponible por el momento")
        }
        
        //open(scheme: hashEntidad[indexPath.row].url.absoluteString)
        
        //        let vc = self.storyboard!.instantiateViewController(withIdentifier: "PlayerVideo") as! PlayerVideo
        //        vc.pathURL = hashEntidad[indexPath.row].url.absoluteString
        //        self.navigationController?.pushViewController(vc, animated: true)
        //
        

    }
    
    // custom function to generate a random UIColor
    func randomColor() -> UIColor{
        let red = CGFloat(drand48())
        let green = CGFloat(drand48())
        let blue = CGFloat(drand48())
        return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
    }
    
    func open(scheme: String) {
        if let url = URL(string: scheme) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                                            print("Open \(scheme): \(success)")
                })
            } else {
                let success = UIApplication.shared.openURL(url)
                print("Open \(scheme): \(success)")
            }
        }
    }
}

