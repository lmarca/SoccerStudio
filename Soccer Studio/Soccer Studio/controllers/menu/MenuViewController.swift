//
//  MenuViewController.swift
//  Culture
//
//  Created by J on 3/23/16.
//  Copyright © 2016 LIMAAPP E.I.R.L. All rights reserved.
//

import UIKit

let inicioTitle = "Home"
let funcionariosTitle = "My Coach"
let portafolioTitle = "Mi Portafolio"
let cotizacionesTitle =  "Video Library"
let fondosTitle = "History"
let emisoresTitle = "Favorites"
let hechosTitle = "Help"
let novedadesTitle =  "My account"


let headerTitle =  "headerTitle"
let footerTitle = "footerTitle"

class MenuViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    
    var previouslyPresentedRow = 0
    
    let inicioImage = "ic_inicio"
    let funcionariosImage = "ic_funcionarios"
    let portafolioImage = "ic_portafolio"
    let cotizacionesImage = "ic_cotizaciones"
    let fondosImage = "ic_fondos"
    let emisoresImage = "ic_emisores"
    let hechosImage = "ic_hechos"
    let novedadesImage = "ic_novedades"
    let orientacionImage = "ic_orientacion"
    let empresasImage = "ic_eye"
    let consultaImage = "ic_consulta"
    

    
    
    
    var homeViewController: UINavigationController?
    var funcionariosViewController: UINavigationController?
    var portafolioViewController: UINavigationController?
    var resultadosViewController: UINavigationController?
    var riesgosViewController: UINavigationController?
    var ubicanosViewController: UINavigationController?
    var sugerenciasViewController: UINavigationController?
    
    var menuItemTitles = [headerTitle, inicioTitle, funcionariosTitle, portafolioTitle, cotizacionesTitle, fondosTitle, emisoresTitle, hechosTitle, novedadesTitle, footerTitle]
    
 
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
        // menuItemTitles.append(consultaTitle) OCULTAR SECCION DE CONSULTA EXPEDIENTE
        
        self.view.backgroundColor = UIColor.backgroundMenu()
        
    }
    
    //MARK: image For Optio nWith Title
    func imageForOptionWithTitle(title:String) -> UIImage? {
        
        var imageName: String!
        
        if (title == inicioTitle) {
            imageName = inicioImage
        }
            
        else if (title == funcionariosTitle) {
            imageName = funcionariosImage
        }
            
        else if (title == portafolioTitle) {
            imageName = portafolioImage
        }
            
        else if (title == cotizacionesTitle) {
            imageName = cotizacionesImage
        }
            
        else if (title == fondosTitle) {
            imageName = fondosImage
        }
            
        else if (title == emisoresTitle) {
            imageName = emisoresImage
        }
            
        else if (title == hechosTitle) {
            imageName = hechosImage
        }
            
        else if (title == novedadesTitle) {
            imageName = novedadesImage
        }
            
        else {
            return nil
        }
        
        return UIImage(named: imageName)
    }
    
    @IBAction func onClickEvent(sender: AnyObject) {
        
        let alertController = UIAlertController(title: nil, message: "Seguro deseas salir?", preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let destroyAction = UIAlertAction(title: "Salir", style: .destructive) { (action) in
            
            OperationQueue.main.addOperation({
                self.removeDirectory()
                self.openLoginScene()
            })
        }
        
        alertController.addAction(destroyAction)
        
        self.present(alertController, animated:true, completion: nil)
    }
    
    
    func openLoginScene() {
        let loginStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let loginViewController = loginStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let navigationViewController = loginStoryboard.instantiateViewController(withIdentifier: "LoginNavController") as! TransparentNavigationController
        navigationViewController.setViewControllers([loginViewController], animated: true)
        self.show(navigationViewController, sender: self)
    }
    
  
}

extension MenuViewController {
    
    func ifExistsImage() -> Bool {
        let filePath = getDocumentsDirectory().appendingPathComponent("logoMenu.png")?.path
        if FileManager.default.fileExists(atPath: filePath!) {
            return true
        }
        return false
    }
    
    func readImage() -> UIImage {
        
        let filePath = getDocumentsDirectory().appendingPathComponent("logoMenu.png")?.path
        if FileManager.default.fileExists(atPath: filePath!) {
            return UIImage(contentsOfFile: filePath!)!
        }
        
        return UIImage()
    }
    
    func getDocumentsDirectory() -> NSURL {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSURL
    }
    
    
    func removeDirectory () {
        
        // Create a FileManager instance
        
        let fileManager = FileManager.default
        
        // Delete 'subfolder' folder
        let filePath = getDocumentsDirectory().appendingPathComponent("UserIcon.png")?.path
        
        do {
            try fileManager.removeItem(atPath: filePath!)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
    }
}

extension MenuViewController: UITableViewDataSource {
    
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItemTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let title = menuItemTitles[indexPath.row]
        
        if title == headerTitle {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath as IndexPath)
           
            cell.selectionStyle = .none
            return cell
            
        }
        else if title == footerTitle {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FooterCell", for: indexPath as IndexPath) as! FooterViewCell
            
            cell.tapAction1 = { (cell) in
                self.showAlertForRow(row: 0)
            }
            
            cell.tapAction2 = { (cell) in
                self.showAlertForRow(row: 1)
            }
           
            cell.selectionStyle = .none
            return cell
        }
        
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuItemCell", for: indexPath as IndexPath) as! MenuItemCell
            cell.menuItemTitle.text = title
            
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor.backgroundMenu()
            
            if title == cotizacionesTitle {
                cell.badge.isHidden = false
            }
            else {
                cell.badge.isHidden = true
            }
            
            //cell.setNeedsLayout()
            return cell
        }
    }
    
    // MARK: - Extracted method
    
    func showAlertForRow(row: Int) {
        
        if row == 0 {
            
            let profileStoryboard = UIStoryboard(name: "Login", bundle: nil)
            
            let nav = profileStoryboard.instantiateViewController(withIdentifier: "SubscribNavController") as! BlackTransparentNavigationController
            let profileViewController = profileStoryboard.instantiateViewController(withIdentifier: "SuscribeVC") as! SuscribeVC
            profileViewController.isModal = true
            nav.setViewControllers([profileViewController], animated: true)
            self.show(nav, sender: self)
        }
        else {
            
            let alertController = UIAlertController(title: nil, message: "Seguro deseas salir?", preferredStyle: .actionSheet)
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            let destroyAction = UIAlertAction(title: "Salir", style: .destructive) { (action) in
                
                OperationQueue.main.addOperation({
                    
                    let profileStoryboard = UIStoryboard(name: "Login", bundle: nil)
                    
                    let nav = profileStoryboard.instantiateViewController(withIdentifier: "loginIdentifier") as! BlackTransparentNavigationController
                    let profileViewController = profileStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    nav.setViewControllers([profileViewController], animated: true)
                    self.show(nav, sender: self)
                })
            }
            
            alertController.addAction(destroyAction)
            
            self.present(alertController, animated:true, completion: nil)
        }
        

    }
}

extension MenuViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView()
        vw.backgroundColor = UIColor.clear
        
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let title = menuItemTitles[indexPath.row]
        
        if title == headerTitle {
            return 130
        }
        else if title == footerTitle {
            return 140
        }else {
            return 60
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let title = menuItemTitles[indexPath.row]
        
        if title == headerTitle || title == footerTitle {
            return
        }
        
        let previouslyPresentedCell = tableView.cellForRow(at: NSIndexPath(row: previouslyPresentedRow, section: 0) as IndexPath)
        
        previouslyPresentedCell?.setSelected(false, animated: false)
        
        let currentCell = tableView.cellForRow(at: indexPath as IndexPath)
        currentCell!.setSelected(true, animated: false)

        var newFrontController : UIViewController!

        
        
        
        //**************************************************
        // MARK: - INICIO
        //**************************************************
        
        if title == inicioTitle {
            
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "MyLocationViewController") as! TransparentNavigationController
            let locationViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            nav.setViewControllers([locationViewController], animated: true)
            self.homeViewController = nav
            newFrontController = self.homeViewController
        }
        
            
        //**************************************************
        // MARK: - FUNCIONARIOS
        //**************************************************
            
        else if title == funcionariosTitle {
            
            
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "MyCoachViewController") as! BlackTransparentNavigationController
            let locationViewController = self.storyboard?.instantiateViewController(withIdentifier: "ListCoach") as! ListCoach
            nav.setViewControllers([locationViewController], animated: true)
            self.homeViewController = nav
            newFrontController = self.homeViewController
            
        }
            
            
        //**************************************************
        // MARK: - PORTAFOLIO
        //**************************************************
            
        else if title == portafolioTitle {
            
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "MyLocationViewController") as! TransparentNavigationController
            let locationViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            nav.setViewControllers([locationViewController], animated: true)
            self.homeViewController = nav
            newFrontController = self.homeViewController
        }
            

        //**************************************************
        // MARK: - COTIZACIONES
        //**************************************************
            
        else if title == cotizacionesTitle {
            
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "MyLocationViewController") as! TransparentNavigationController
            let locationViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            nav.setViewControllers([locationViewController], animated: true)
            self.homeViewController = nav
            newFrontController = self.homeViewController
        }
            
        
            
        //**************************************************
        // MARK: - FONDOS MUTUOS
        //**************************************************
            
        else if title == fondosTitle {
            
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "MyLocationViewController") as! TransparentNavigationController
            let locationViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            nav.setViewControllers([locationViewController], animated: true)
            self.homeViewController = nav
            newFrontController = self.homeViewController
            
//            if let locationViewController = self.riesgosViewController {
//                newFrontController = locationViewController
//            }
//
//            else {
//
//                let tabBarController = self.storyboard?.instantiateViewController(withIdentifier: "tabBarController") as! UITabBarController
//                tabBarController.selectedIndex = 3
//                newFrontController = tabBarController
//            }
        }
        
        
            
        //**************************************************
        // MARK: - EMISORES
        //**************************************************
            
        else if title == emisoresTitle {
            
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "MyLocationViewController") as! TransparentNavigationController
            let locationViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            nav.setViewControllers([locationViewController], animated: true)
            self.homeViewController = nav
            newFrontController = self.homeViewController
        }
            
       
            
        //**************************************************
        // MARK: - HECHOS DE IMPORTANCIA
        //**************************************************
        
        else if title == hechosTitle {
            
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "MyLocationViewController") as! TransparentNavigationController
            let locationViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            nav.setViewControllers([locationViewController], animated: true)
            self.homeViewController = nav
            newFrontController = self.homeViewController
        }
        
            
        
        //**************************************************
        // MARK: - NOVEDADES
        //**************************************************
            
        else if title == novedadesTitle {
            
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "MyLocationViewController") as! TransparentNavigationController
            let locationViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            nav.setViewControllers([locationViewController], animated: true)
            self.homeViewController = nav
            newFrontController = self.homeViewController
        }
        
            
        
        //**************************************************
        // MARK: - ORIENTACION
        //**************************************************
//
//        else if title == orientacionTitle {
//
//        }
        

        

        revealViewController().pushFrontViewController(newFrontController, animated: true)

        previouslyPresentedRow = indexPath.row
        
    }
}


