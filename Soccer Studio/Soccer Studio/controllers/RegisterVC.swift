//
//  RegisterVC.swift
//  Soccer Studio
//
//  Created by APPLE on 15/05/18.
//  Copyright © 2018 Oxicode. All rights reserved.
//

import UIKit
import TransitionButton

class RegisterVC: myActivity {
    
    @IBOutlet weak var button: TransitionButton!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
       self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_flecha_atras"), style: .plain, target: self, action: #selector(self.pop))

        self.title = "Login"
        
        button.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonAction(_ button: TransitionButton) {
        
        button.startAnimation()
        
        let qualityOfServiceClass = DispatchQoS.QoSClass.background
        let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
        backgroundQueue.async(execute: {
            
            sleep(5) // 3: Do your networking task or background work here.
            
            DispatchQueue.main.async(execute: { () -> Void in
                // 4: Stop the animation, here you have three options for the `animationStyle` property:
                // .expand: useful when the task has been compeletd successfully and you want to expand the button and transit to another view controller in the completion callback
                // .shake: when you want to reflect to the user that the task did not complete successfly
                // .normal
                button.stopAnimation(animationStyle: .expand, completion: {
                    //let secondVC = HomeTopicsVC()
                    //self.present(secondVC, animated: true, completion: nil)
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateInitialViewController()!
                    self.present(controller, animated: true, completion: nil)
                })
            })
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

