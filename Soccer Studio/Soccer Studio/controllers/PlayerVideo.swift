//
//  PlayerVideo.swift
//  Soccer Studio
//
//  Created by APPLE on 5/06/18.
//  Copyright © 2018 Oxicode. All rights reserved.
//

import UIKit

class PlayerVideo: UIViewController {
    
    var loadingView : UIActivityIndicatorView!
    @IBOutlet weak var webView: UIWebView!
    var pathURL: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let x = Int(UIScreen.main.bounds.width / 2 - 25)
        loadingView = UIActivityIndicatorView(frame : CGRect(x: x, y: 100, width: 50, height: 50))
        loadingView.activityIndicatorViewStyle = .gray
        
        webView.delegate = self
        
        if let url = URL(string: pathURL) {
            let request = URLRequest(url: url)
            webView.loadRequest(request)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
        
        view.addSubview(loadingView)
        loadingView.startAnimating()

        // Do any additional setup after loading the view.
    }

    static func initController() -> PlayerVideo {
        
        let vc = PlayerVideo()
        return vc
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PlayerVideo : UIWebViewDelegate {
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        loadingView.removeFromSuperview()
    }
    
}
