//
//  UIColor.swift
//  Sercotec
//
//  Created by APPLE on 12/12/17.
//  Copyright © 2017 OxIcOdE. All rights reserved.
//

import UIKit

extension UIColor {

    public convenience init(hexString: String) {
        let hexString = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) as String
        let scanner = Scanner(string: hexString)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
    
    static func quickColorWith(red:Int, green:Int, blue:Int, alpha:CGFloat) -> UIColor {
        return UIColor(red: CGFloat(red)/255.0, green: CGFloat(green)/255.0, blue: CGFloat(blue)/255.0, alpha: alpha)
    }
    
    static func appColor() -> UIColor {
        return UIColor.quickColorWith(red: 99, green: 26, blue: 95, alpha: 1.0)
    }
    
    static func backgroundMenu() -> UIColor {
        return UIColor.quickColorWith(red: 19, green: 50, blue: 42, alpha: 1.0)
    }
    
    static func watermelonColor() -> UIColor {
        return UIColor.quickColorWith(red: 251, green: 52, blue: 75, alpha: 1.0)
    }
    
    static func backgroundBlack() -> UIColor {
        return UIColor(red: 29.0/255.0, green: 33.0/255.0, blue: 35.0/255.0, alpha: 0.8)
    }
    
    static func darkGreyDefaultColor() -> UIColor  {
        return UIColor.quickColorWith(red: 153, green: 153, blue: 153, alpha: 1.0)
    }
    
    static func lightGreyDefaultColor() -> UIColor  {
        return UIColor.quickColorWith(red: 204, green: 204, blue: 204, alpha: 1.0)
    }
    
    static func greyDefaultColor() -> UIColor  {
        return UIColor.quickColorWith(red: 102, green: 102, blue: 102, alpha: 1.0)
    }
    
    static func grayTextfieldBackground() -> UIColor {
        return UIColor.quickColorWith(red: 85, green: 85, blue: 85, alpha: 1.0)
    }
    
    static func selectedButtonColor() -> UIColor  {
        return UIColor.quickColorWith(red: 165, green: 165, blue: 165, alpha: 1.0)
    }
    
    static func unselectedButtonColor() -> UIColor  {
        return UIColor.quickColorWith(red: 165, green: 165, blue: 165, alpha: 1.0)
    }
    
    //Category Colors
    
    static func urbanCategoryColor() -> UIColor {
        return UIColor.quickColorWith(red: 96, green: 66, blue: 168, alpha: 1.0)
    }
    
    static func buttonSelected() -> UIColor {
        return UIColor.quickColorWith(red: 32, green: 55, blue: 71, alpha: 1.0)
    }
    
    static func theaterCategoryColor() -> UIColor {
        return UIColor.quickColorWith(red: 119, green: 159, blue: 40, alpha: 1.0)
    }
    
    static func peintureCategoryColor() -> UIColor {
        return UIColor.quickColorWith(red: 99, green: 176, blue: 223, alpha: 1.0)
    }
    
    static func performanceCategoryColor() -> UIColor {
        return UIColor.quickColorWith(red: 138, green: 168, blue: 88, alpha: 1.0)
    }
    
    static func patrimonioCategoryColor() -> UIColor {
        return UIColor.quickColorWith(red: 210, green: 149, blue: 136, alpha: 1.0)
    }
    
    static func musicCategoryColor() -> UIColor {
        return UIColor.quickColorWith(red: 215, green: 139, blue: 44, alpha: 1.0)
    }
    
    static func liteatureCategoryColor() -> UIColor {
        return UIColor.quickColorWith(red: 93, green: 163, blue: 113, alpha: 1.0)
    }
    
    static func instalacionCategoryColor() -> UIColor {
        return UIColor.quickColorWith(red: 142, green: 202, blue: 179, alpha: 1.0)
    }
    
    static func ilustracionCategoryColor() -> UIColor {
        return UIColor.quickColorWith(red: 183, green: 27, blue: 93, alpha: 1.0)
    }
    
    static func photographyCategoryColor() -> UIColor {
        return UIColor.quickColorWith(red: 222, green: 79, blue: 41, alpha: 1.0)
    }
    
    static func scultureCategoryColor() -> UIColor {
        return UIColor.quickColorWith(red: 142, green: 210, blue: 231, alpha: 1.0)
    }
    
    static func designCategoryColor() -> UIColor {
        return UIColor.quickColorWith(red: 223, green: 96, blue: 89, alpha: 1.0)
    }
    
    static func danceCategoryColor() -> UIColor {
        return UIColor.quickColorWith(red: 201, green: 129, blue: 162, alpha: 1.0)
    }
    
    static func circusCategoryColor() -> UIColor {
        return UIColor.quickColorWith(red: 179, green: 37, blue: 32, alpha: 1.0)
    }
    
    static func cineCategoryColor() -> UIColor {
        return UIColor.quickColorWith(red: 229, green: 158, blue: 54, alpha: 1.0)
    }
    
    static func audiovisualCategoryColor() -> UIColor {
        return UIColor.quickColorWith(red: 87, green: 106, blue: 173, alpha: 1.0)
    }
    
    static func architectureCategoryColor() -> UIColor {
        return UIColor.quickColorWith(red: 190, green: 193, blue: 210, alpha: 1.0)
    }
    
    //Basic Colors
    
    
    static func bannerColorBlue() -> UIColor {
        return UIColor.quickColorWith(red: 29, green: 102, blue: 170, alpha: 1.0)
    }
    
    static func bannerColorGreen() -> UIColor {
        return UIColor.quickColorWith(red: 126, green: 174, blue: 57, alpha: 1.0)
    }
    
    static func bannerColorTitle() -> UIColor {
        return UIColor.quickColorWith(red: 107, green: 77, blue: 23, alpha: 1.0)
    }
    
    static func titleColor() -> UIColor {
        return UIColor.quickColorWith(red: 99, green: 26, blue: 95, alpha: 1.0)
    }
    
    static func transparentBlack() -> UIColor {
        return UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.8)
    }
    
}
