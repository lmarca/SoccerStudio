//
//  CCAnimations.swift
//  PopAnimation
//
//  Created by Carlos Corrêa on 10/04/16.
//  Copyright © 2016 Carlos Corrêa. All rights reserved.
//



import UIKit

enum AnimationKeyPath:String {
    case
    scale        = "transform.scale",
    zRotation    = "transform.rotation.z",
    xPosition    = "position.x",
    yPosition    = "position.y",
    xTranslation = "translation.x",
    transform    = "transform",
    opacity      = "opacity"
}

enum Axis {
    case
    x,
    y,
    z
}

enum Side {
    case
    Up,
    Down,
    Left,
    Right
}

// MARK: helpers

func degreeToRadians(angle:Float) -> Float {
    return (angle / 180 * Float(M_PI))
}

extension UIView {
    
    // MARK: - Setup Animation functions
    
    private func setupBasicAnimation(animation:CABasicAnimation,
                                     duration:CFTimeInterval,
                                     fromValue:AnyObject? = nil,
                                     toValue:AnyObject? = nil,
                                     byValue:AnyObject? = nil,
                                     cumulative: Bool = false,
                                     reverse: Bool = false,
                                     repeatCount:Float = 0,
                                     fillMode:String = kCAFilterLinear,
                                     removedOnCompletion:Bool = true) {
        
        animation.duration = duration
        animation.isCumulative = cumulative
        animation.repeatCount = repeatCount
        animation.fromValue = fromValue
        animation.toValue = toValue
        animation.byValue = byValue
        animation.autoreverses = reverse
        animation.fillMode = fillMode
        animation.isRemovedOnCompletion = removedOnCompletion
    }
    
    private func setupKeyFrameAnimation(animation:CAKeyframeAnimation,
                                        values:[AnyObject]?,
                                        keyTimes:[AnyObject]?,
                                        duration:CFTimeInterval) {
        animation.values = values
        animation.keyTimes = keyTimes as? [NSNumber] ?? []
        animation.duration = duration
    }
    
    private func setupSpringAnimation(animation:CASpringAnimation,
                                      damping:CGFloat = 1.0,
                                      initialVelocity:CGFloat = 1.0,
                                      speed:Float = 1.0) {
        animation.damping = damping
        animation.initialVelocity = initialVelocity
        animation.speed = speed
    }
    
    private func runAnimation(animation:CAAnimation) {
        self.layer.add(animation, forKey: nil)
    }
    
    // MARK: - Factory
    
    private func newAnimation<T:CAAnimation>(type:T.Type, keyPath:AnimationKeyPath) -> T {
        if type == CABasicAnimation.self {
            return basicAnimation(keyPath: keyPath) as! T
        } else if type == CAKeyframeAnimation.self {
            return keyFrameAnimation(keyPath: keyPath) as! T
        } else if type == CASpringAnimation.self {
            return springAnimation(keyPath: keyPath) as! T
        }
        return CAAnimation() as! T
    }

    private func basicAnimation(keyPath: AnimationKeyPath) -> CABasicAnimation {
        return CABasicAnimation(keyPath: keyPath.rawValue)
    }
    
    private func keyFrameAnimation(keyPath: AnimationKeyPath) -> CAKeyframeAnimation {
        return CAKeyframeAnimation(keyPath: keyPath.rawValue)
    }
    
    private func springAnimation(keyPath: AnimationKeyPath) -> CASpringAnimation {
        return CASpringAnimation(keyPath: keyPath.rawValue)
    }
    
    // MARK: - Animation Helpers
    
    private func scaleAnimation(size:CGFloat, duration:CFTimeInterval) {
        let animation = newAnimation(type: CASpringAnimation.self, keyPath: .scale)
        setupBasicAnimation(animation: animation, duration: duration, toValue:size as AnyObject?, reverse:true)
        setupSpringAnimation(animation: animation, damping: 7.0, initialVelocity: 1.1, speed: 1.7)
        runAnimation(animation: animation)
    }
    
    private func shakeAnimation(duration:CFTimeInterval) {
        let centerPoint = self.center
        let animationValues = [centerPoint.x, centerPoint.x + 5, centerPoint.x - 5, centerPoint.x + 5, centerPoint.x]
        let keyTimes = [0, 0.25, 0.75, 1.25, 1]
        let animation = newAnimation(type: CAKeyframeAnimation.self, keyPath: .xPosition)
        setupKeyFrameAnimation(animation: animation, values: animationValues as [AnyObject]?, keyTimes: keyTimes as [AnyObject]?, duration: duration)
        runAnimation(animation: animation)
    }
    
    private func rotate360(repeatCount:Float, duration:CFTimeInterval) {
        let animation = newAnimation(type: CABasicAnimation.self, keyPath: .zRotation)
        let repeatCount = repeatCount == 0 ? Float(CGFloat.greatestFiniteMagnitude) : repeatCount
        setupBasicAnimation(animation: animation, duration:duration, toValue:M_PI_2 as AnyObject?, repeatCount:repeatCount)
        runAnimation(animation: animation)
    }
    
    private func jumpWithIntensity(withIntensity intensity:CGFloat, duration:CFTimeInterval) {
        let currentPoint = self.center
        let fromValue = currentPoint.y
        let toValue = fromValue - intensity
        let animation = newAnimation(type: CASpringAnimation.self, keyPath: .yPosition)
        setupBasicAnimation(animation: animation, duration: duration, toValue:toValue as AnyObject?, reverse:true)
        setupSpringAnimation(animation: animation, damping: 8.2, initialVelocity: 1.2, speed: 1.1)
        runAnimation(animation: animation)
    }
    
    private func rotate3DWithAxis(axis:Axis, withDuration duration:CFTimeInterval) {
        var transform3D:CATransform3D
        
        if axis == .x {
            transform3D = CATransform3DRotate(self.layer.transform, CGFloat(degreeToRadians(angle: 180)), 0.0, 1.0, 0.0)
        } else {
            transform3D = CATransform3DRotate(self.layer.transform, CGFloat(degreeToRadians(angle: 180)), 1.0, 0.0, 0.0)
        }
    
        let animation = newAnimation(type: CABasicAnimation.self, keyPath: .transform)
        setupBasicAnimation(animation: animation, duration: duration, toValue: NSValue(caTransform3D:transform3D), cumulative:true, repeatCount:2, fillMode:kCAFillModeForwards, removedOnCompletion:false)
        runAnimation(animation: animation)
    }
    
    private func moveSideways(WithDuration duration:CFTimeInterval, side:Side, value:CGFloat) {
        var keyPath:AnimationKeyPath
        var toValue:CGFloat = 0.0
        switch side {
        case .Up,
             .Down:
            keyPath = .yPosition
            toValue = self.center.y
            break
        case .Left,
             .Right:
            keyPath = .xPosition
            toValue = self.center.x
            break
        }
        
        if (side == .Left || side == .Up) {
            toValue -= value
        } else {
            toValue += value
        }
        
        keyPath = .opacity
        toValue = 1.0
        
        let animation = newAnimation(type: CABasicAnimation.self, keyPath: keyPath)
        setupBasicAnimation(animation: animation, duration: duration, toValue:toValue as AnyObject?)
        runAnimation(animation: animation)
    }
    
    private func alphaAnimation(withDuration duration:CFTimeInterval, withAlpha alpha:CGFloat) {
        let animation = newAnimation(type: CABasicAnimation.self, keyPath: .opacity)
        setupBasicAnimation(animation: animation, duration: duration, toValue:alpha as AnyObject?)
        runAnimation(animation: animation)
    }
    
    
    // MARK: - Custom animations
    
    /**
     This will create a pop animation by growing or shrinking the view scale size and then going back to the original state
     
     - parameter duration:    The duration of the animation
     - parameter scale: The scale size that the view will resize to (1.0 is the original scale)
     */
    func CCAnimationPop(duration:CFTimeInterval = 0.3, scale:CGFloat = 1.05) {
        scaleAnimation(size: scale, duration: duration)
    }
    
    /**
     This will create a shake animation by moving the view x position to the left and right
     
     - parameter duration:    The duration of the animation
     */
    func CCAnimationShake(duration:CFTimeInterval = 0.4) {
        shakeAnimation(duration: duration)
    }
    
    /**
     This will create a 360 degrees rotation animation
     
     - parameter duration:    The duration of the animation
     - parameter repeatCount: The number of times that the animation will repeat. 0 stands for infinite.
     */
    func CCAnimationRotate(duration:CFTimeInterval = 0.4, repeatCount:Float = 1) {
        rotate360(repeatCount: repeatCount, duration: duration)
    }
    
    /**
     This will create a jump-like animation, by changing the Y position.
     
     - parameter duration: The animation duration
     - parameter intensity: how much the view will be moved in the Y axis.
     */
    func CCAnimationJump(duration:CFTimeInterval = 0.3, intensity:CGFloat) {
        jumpWithIntensity(withIntensity: intensity, duration: duration)
    }
    
    /**
     This will create a X axis rotation animation by 360 degrees.
     
     - parameter duration: The animation duration
     */
    func CCAnimation3DxRotation(duration:CFTimeInterval = 0.4) {
        rotate3DWithAxis(axis: .x, withDuration: duration)
    }
    
    /**
     This will create a Y axis rotation animation by 360 degrees.
     
     - parameter duration: The animation duration
     */
    func CCAnimation3DyRotation(duration:CFTimeInterval = 0.4) {
        rotate3DWithAxis(axis: .y, withDuration: duration)
    }
    
    /**
     This will create a jump and spin on X axis animation. It's a union of CCAnimationJump and CCJumpTwistXAnimation
     
     - parameter duration: The animation duration
     - parameter intensity: how much the view will be moved in the Y axis.
     */
    func CCAnimationJumpTwistX(duration:CFTimeInterval = 0.4, intensity:CGFloat) {
        CCAnimationJump(duration: duration, intensity: intensity)
        CCAnimation3DxRotation(duration: duration)
    }
    
    /**
     This will create a jump and spin on Y axis animation. It's a union of CCAnimationJump and CCJumpTwistYAnimation
     
     - parameter duration: The animation duration
     - parameter intensity: how much the view will be moved in the Y axis.
     */
    func CCAnimationJumpTwistY(duration:CFTimeInterval = 0.4, intensity:CGFloat) {
        CCAnimationJump(duration: duration, intensity: intensity)
        CCAnimation3DyRotation(duration: duration)
    }
    
    /**
     This will create a fade animation while moving the view to the left.
     
     - parameter duration:  The animation duration
     - parameter intensity: how much the view will be moved to the left.
     */
    func CCAnimationFadeToLeft(duration:CFTimeInterval = 0.4, intensity:CGFloat) {
        moveSideways(WithDuration: duration, side: .Left, value: intensity)
        alphaAnimation(withDuration: duration, withAlpha: 0)
    }
    
    /**
     This will create a fade animation while moving the view to the right.
     
     - parameter duration:  The animation duration
     - parameter intensity: how much the view will be moved to the tight.
     */
    func CCAnimationFadeToRight(duration:CFTimeInterval = 0.4, intensity:CGFloat) {
        moveSideways(WithDuration: duration, side: .Right, value: intensity)
        alphaAnimation(withDuration: duration, withAlpha: 0)
    }
    
    /**
     This will create a fade animation while moving the view to the top of the screen.
     
     - parameter duration:  The animation duration
     - parameter intensity: how much the view will be moved to the top of the screen.
     */
    func CCAnimationFadeUp(duration:CFTimeInterval = 1.0, intensity:CGFloat) {
        //moveSideways(WithDuration: duration, side: .Up, value: intensity)
        alphaAnimation(withDuration: duration, withAlpha: 1)
    }
    
    /**
     This will create a fade animation while moving the view to the bottom of the screen.
     
     - parameter duration:  The animation duration
     - parameter intensity: how much the view will be moved to the bottom of the screen.
     */
    func CCAnimationFadeDown(duration:CFTimeInterval = 0.4, intensity:CGFloat) {
        moveSideways(WithDuration: duration, side: .Down, value: intensity)
        alphaAnimation(withDuration: duration, withAlpha: 0)
    }
}
