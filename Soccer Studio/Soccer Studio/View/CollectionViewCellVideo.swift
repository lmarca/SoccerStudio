//
//  CollectionViewCellVideo.swift
//  Soccer Studio
//
//  Created by APPLE on 6/06/18.
//  Copyright © 2018 Oxicode. All rights reserved.
//

import UIKit

class CollectionViewCellVideo: UICollectionViewCell {
    
    
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var info: UIImageView!
    @IBOutlet weak var titulo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        container.backgroundColor = UIColor.white
        container.layer.shadowOffset = CGSize.zero
        container.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.5).cgColor
        container.layer.shadowOpacity = 1
        container.layer.shadowRadius = 5
    }

}
