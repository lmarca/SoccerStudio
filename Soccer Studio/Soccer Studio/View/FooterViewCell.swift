//
//  FooterViewCell.swift
//  Soccer Studio
//
//  Created by APPLE on 5/06/18.
//  Copyright © 2018 Oxicode. All rights reserved.
//

import UIKit

class FooterViewCell: UITableViewCell {
    
    var tapAction1: ((UITableViewCell) -> Void)?
    var tapAction2: ((UITableViewCell) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func button1(_ sender: Any) {
        tapAction1?(self)
    }
    
    @IBAction func button2(_ sender: Any) {
        tapAction2?(self)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
