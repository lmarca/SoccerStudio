//
//  CollectionViewCell.swift
//  Soccer Studio
//
//  Created by APPLE on 22/05/18.
//  Copyright © 2018 Oxicode. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nombre: UILabel!
    @IBOutlet weak var container: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //container.layer.cornerRadius = 6
        container.backgroundColor = UIColor.white
        container.layer.shadowOffset = CGSize.zero
        container.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.5).cgColor
        container.layer.shadowOpacity = 1
        container.layer.shadowRadius = 5
    }

}
